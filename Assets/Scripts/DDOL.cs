﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDOL : MonoBehaviour {

	/// <summary>
	/// This is the only place in the whole project you use DontDestroyOnLoad
	/// </summary>
	public void Awake()
	{
		DontDestroyOnLoad(gameObject);
        // Call something from Manager Class just to initialize it
        transform.position = Manager.soundman.transform.position;
	}
}

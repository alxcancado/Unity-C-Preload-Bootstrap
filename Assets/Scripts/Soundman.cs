﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundman : MonoBehaviour {
	AudioSource _audioSource;
	AudioSource audioSource
	{
		get
		{
			if(_audioSource == null)
				_audioSource = FindObjectOfType<AudioSource>();
				Debug.Log("AudioSource: "+FindObjectOfType<AudioSource>());

			return _audioSource;
		}
	}
	[SerializeField] private AudioClip sound1;
	[SerializeField] private AudioClip sound2;
	[SerializeField] private AudioClip sound3;

	public void PlaySpeedItem()
	{
		Debug.Log(audioSource);
		Debug.Log("PlaySpeedItem()");
		audioSource.PlayOneShot (this.sound1,1f);
		//AudioSource.PlayClipAtPoint (sound1, transform.position);
		//AudioSource.PlayClipAtPoint (this.sound1, this.transform.position);
	}

	public void PlaySlowItem()
	{
		Debug.Log("PlaySlowItem()");
		//audioSource.PlayOneShot (this.sound2,1f);
		//AudioSource.PlayClipAtPoint (hit, transform.position);
	}

	public void PlayDestroyItem()
	{
		Debug.Log("PlayDestroyItem()");
		//audioSource.PlayOneShot (this.sound3,1f);
		//AudioSource.PlayClipAtPoint (hit, transform.position);
	}
}
